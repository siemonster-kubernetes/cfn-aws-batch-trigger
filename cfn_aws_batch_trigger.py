#!/usr/bin/env python
from crhelper import CfnResource
import boto3
import os
import logging

logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL')

try:
    session = boto3.session.Session()
    client = session.client('batch')

    JOB_QUEUE = os.environ['JobQueue']
    JOB_DEF = os.environ['JobDefinition']
    DESTROY_JOB_DEF = os.environ['DestroyJobDefinition']
except Exception as e:
    helper.init_failure(e)


def get_job_name(event, prefix='Deploy'):
    try:
        STACK_NAME = event['ResourceProperties']['StackName']
        REQUEST_ID = event['RequestId']
        return "%s-%s-%s" % (STACK_NAME, prefix, REQUEST_ID)
    except Exception as e:
        helper.init_failure(e)


def get_jobs_list():
    jobs = []
    for status in ['SUBMITTED', 'PENDING', 'RUNNABLE', 'STARTING', 'RUNNING', 'SUCCEEDED', 'FAILED']:
        response = client.list_jobs(
            jobQueue=JOB_QUEUE,
            jobStatus=status,
        )
        for job in response['jobSummaryList']:
            jobs.append(job)
            # print("%s: %s" %( status, job))
    return jobs


def get_job_by_name(job_name, jobs_list=None):
    for job in jobs_list or get_jobs_list():
        if job['jobName'] == job_name:
            return job


@helper.create
def create(event, context):
    logger.info("Got Create")
    # Optionally return an ID that will be used for the resource PhysicalResourceId,
    # if None is returned an ID will be generated. If a poll_create function is defined
    # return value is placed into the poll event as event['CrHelperData']['PhysicalResourceId']
    #
    # To add response data update the helper.Data dict
    # If poll is enabled data is placed into poll event as event['CrHelperData']
    # Grab data from environment

    # Create unique name for the job (this does not need to be unique)
    job_name = get_job_name(event)

    # Submit the job
    job1 = client.submit_job(
        jobName=job_name,
        jobQueue=JOB_QUEUE,
        jobDefinition=JOB_DEF
    )
    logger.info("Started Job: {}".format(job1['jobName']))

    helper.Data.update({
        'jobName': job1['jobName'],
        'jobId': job1['jobId'],
    })
    return job1['jobName']


@helper.update
def update(event, context):
    logger.info("Got Update")
    # If the update resulted in a new resource being created, return an id for the new resource. CloudFormation will send
    # a delete event with the old id when stack update completes


@helper.delete
def delete(event, context):
    logger.info("Got Delete")

    job_name = get_job_name(event, prefix='Destroy')

    # Submit the job
    job1 = client.submit_job(
        jobName=job_name,
        jobQueue=JOB_QUEUE,
        jobDefinition=DESTROY_JOB_DEF
    )
    logger.info("Started Job: {}".format(job1['jobName']))


@helper.poll_create
def poll_create(event, context):
    logger.info("Got create poll")
    # Return a resource id or True to indicate that creation is complete. if True is returned an id will be generated
    jobs_list = get_jobs_list()
    job_name = get_job_name(event)
    job = get_job_by_name(job_name, jobs_list)
    logger.info(job)
    if not job:
        return

    if job.get('status') == 'SUCCEEDED':
        return True

    if job.get('status') == 'FAILED':
        helper.init_failure("Status: FAILED")


@helper.poll_delete
def poll_delete(event, context):
    logger.info("Got delete poll")

    # Return a resource id or True to indicate that creation is complete. if True is returned an id will be generated
    jobs_list = get_jobs_list()
    job_name = get_job_name(event, prefix='Destroy')
    job = get_job_by_name(job_name, jobs_list)
    logger.info(job)
    if not job:
        return

    if job.get('status') == 'SUCCEEDED' or job.get('status') == 'FAILED':
        return True


def handler(event, context):
    helper(event, context)
